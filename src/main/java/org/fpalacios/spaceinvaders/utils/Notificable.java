package org.fpalacios.spaceinvaders.utils;

@FunctionalInterface
public interface Notificable<T> {
    public void recivirEvento(T evento);
}
