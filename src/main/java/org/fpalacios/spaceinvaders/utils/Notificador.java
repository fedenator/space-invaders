package org.fpalacios.spaceinvaders.utils;

import java.util.List;
import java.util.ArrayList;

/**
 * Un objeto que puede emitir eventos a todos subscriptores que se hayan subscritos
 */
public class Notificador<T> {

    public final List< Notificable<T> > notificables = new ArrayList<>();

    public void subscribir(Notificable<T> notificable) {
        this.notificables.add(notificable);
    }

    public void desuscribir(Notificable<T> notificable) {
        this.notificables.remove(notificable);
    }

    public void emitir(T evento) {
        for (Notificable<T> notificable : this.notificables) {
            notificable.recivirEvento(evento);
        }
    }
}
