package org.fpalacios.spaceinvaders.interfaz.organizadores;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * Panel que alinea los elementos de forma vertical
 */
public class PanelV extends JPanel {

    /*--------------------- Constantes ----------------------*/
    private static final long serialVersionUID = 42l;

    /*------------------- Constructores ---------------------*/
    public PanelV() {
        this.setLayout( new BoxLayout(this, BoxLayout.Y_AXIS) );
    }
}
