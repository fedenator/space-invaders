package org.fpalacios.spaceinvaders.interfaz;

import java.util.Optional;

import java.awt.BorderLayout;

import javax.swing.JFrame;


import org.fpalacios.spaceinvaders.interfaz.vistas.Vista;

/**
 * Ventana principal del juego
 */
public class VentanaPrincipal extends JFrame {

    /*---------------- Constantes -----------------------*/
    private static final long serialVersionUID = 42l;

    /*------------------- Atributos ---------------------*/
    //La vista que se esta mostrando actualmente
    //Cuando se inicializa la vista esta vacia
    private Optional<Vista> vistaActual = Optional.empty();

    /*---------------- Constructores --------------------*/
    public VentanaPrincipal(String title) {
        super(title);

        this.setSize                 ( 600, 400             );
        this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        this.setLayout               ( new BorderLayout()   );

        this.setVisible(true);
    }

    /*---------------------- Funciones ------------------*/
    public void cambiarVistaActual(Vista vistaNueva) {
        if ( this.vistaActual.isPresent() )
            this.getContentPane().remove( this.vistaActual.get() );

        this.vistaActual = Optional.of(vistaNueva);
        this.getContentPane().add(vistaNueva, BorderLayout.CENTER);

        this.revalidate();
    }
}
