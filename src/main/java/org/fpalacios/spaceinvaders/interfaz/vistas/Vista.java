package org.fpalacios.spaceinvaders.interfaz.vistas;

import javax.swing.JPanel;


import org.fpalacios.spaceinvaders.Aplicacion;

/**
 * Reprecentacion de una vista
 */
public abstract class Vista extends JPanel {

    /*---------------------- Constantes -----------------------------*/
    private static final long serialVersionUID = 42l;

    /*---------------------- Atributos ------------------------------*/
    protected Aplicacion aplicacion;

    /*---------------------- Constructores --------------------------*/
    public Vista(Aplicacion aplicacion) {
        this.aplicacion = aplicacion;
    }
}
