package org.fpalacios.spaceinvaders.interfaz.vistas;

import javax.swing.JLabel;
import javax.swing.JButton;


import org.fpalacios.spaceinvaders.Aplicacion;

import org.fpalacios.spaceinvaders.interfaz.organizadores.PanelV;

/**
 * Vista del menu de entrada del juego
 */
public class MenuPrincipal extends Vista {

    /*------------------------- Constantes -----------------------*/
    private static final long serialVersionUID = 42l;

    /*------------------------ Atributos -------------------------*/
    private JLabel  lblTitulo;

    private JButton btnPartida;
    private JButton btnConfiguracion;
    private JButton btnSalir;

    /*---------------------- Constructores -----------------------*/
    public MenuPrincipal(Aplicacion aplicacion) {
        super(aplicacion);

        this.lblTitulo = new JLabel("Menu Principal");

        //Boton de iniciar partida
        this.btnPartida = new JButton("Iniciar partida");
        this.btnPartida.addActionListener( (action) -> {
            this.aplicacion.iniciarPartida();
        });

        //Boton de ir a la vista de configuracion
        this.btnConfiguracion = new JButton("Configuracion");
        this.btnConfiguracion.addActionListener( (action) -> {
            //TODO: [fpalacios] implementar ir a la vista de configuracion
            System.out.println("btnConfiguracion clickeado");
        });

        //Boton de salir
        this.btnSalir = new JButton("Salir");
        this.btnSalir.addActionListener( (action) -> {
            System.exit(0);
        });

        PanelV pnlBotones = new PanelV();

        pnlBotones.add( btnPartida       );
        pnlBotones.add( btnConfiguracion );
        pnlBotones.add( btnSalir         );

        this.add( lblTitulo  );
        this.add( pnlBotones );
    }
}
