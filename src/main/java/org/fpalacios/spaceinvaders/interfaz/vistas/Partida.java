package org.fpalacios.spaceinvaders.interfaz.vistas;

import java.awt.EventQueue;
import org.fpalacios.spaceinvaders.core.SpaceInvaders;
import java.awt.BorderLayout;

import java.awt.Dimension;

import org.fpalacios.spaceinvaders.Aplicacion;
import org.fpalacios.spaceinvaders.motor.MotorDeJuego;
import org.fpalacios.spaceinvaders.interfaz.componentes.LienzoPartida;

// TODO: [fpalacios] pensar un nombre mas descriptivo
/**
 * Vista que muestra una partida
 */
public class Partida extends Vista {

    /*------------------ Constantes -------------------------*/
    private static final long serialVersionUID = 42l;

    /*------------------- Atributos -------------------------*/
    private LienzoPartida lienzo;
    private MotorDeJuego  motorDeJuego;
    private SpaceInvaders juego;

    /*------------------ Constructores ----------------------*/
    public Partida(Aplicacion aplicacion) {
        super(aplicacion);

        this.prepararUI();

        this.motorDeJuego = new MotorDeJuego (this.lienzo      );
        this.juego        = new SpaceInvaders(this.motorDeJuego);

        this.addKeyListener( this.motorDeJuego.teclado );

        this.motorDeJuego.empezar();
    }

    /*------------------- Funciones ------------------------*/
    private void prepararUI() {
        this.setPreferredSize( new Dimension(600, 400) );
        this.setLayout       ( new BorderLayout()      );
        this.setFocusable    ( true                    );

        // En el focus subsystem de java para poder ganar el foco
        // toda la interfaz debe estar agregada y ser visible
        // por eso se espera a la siguiente tarea de el {@link EventQueue}
        // para tratar de ganar el foco
        EventQueue.invokeLater( () -> {
            this.grabFocus();
            this.requestFocusInWindow();
        });

        this.lienzo = new LienzoPartida();

        this.add(this.lienzo, BorderLayout.CENTER);
    }
}
