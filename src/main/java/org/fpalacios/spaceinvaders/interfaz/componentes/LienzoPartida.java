package org.fpalacios.spaceinvaders.interfaz.componentes;

import java.util.Optional;

import java.lang.reflect.InvocationTargetException;

import java.awt.Graphics;
import java.awt.EventQueue;

import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import org.fpalacios.spaceinvaders.motor.Pintor;

/**
 * Componente donde se pinta el juego
 */
public class LienzoPartida extends JComponent implements Pintor {

    /*----------------------- Constantes ----------------------------*/
    private static final long serialVersionUID = 42l;

    /*----------------------- Atributos -----------------------------*/
    //Frame que manda el motor a pintar, solo se debe actualizar
    //desde el hilo de {@link EventQueue} para evitar dataraces con el pintado.
    //Puede estar vacio si el motor todavia no mando ningun frame
    private Optional<BufferedImage> imagen = Optional.empty();

    /*---------------------- Constructores -------------------------*/
    public LienzoPartida() {
        this.setOpaque(true);
    }

    /*------------------------- Funciones --------------------------*/
    @Override
    public void pintar(final BufferedImage imagen) {
        try {
            EventQueue.invokeAndWait( () -> {
                this.imagen = Optional.of(imagen);

                this.paintImmediately( 0, 0, this.getWidth(), this.getHeight() );
            });
        } catch (InterruptedException | InvocationTargetException e) {
            // TODO: [fpalacios] manejar mejor este error
            e.printStackTrace();
        }
    }

    //Funcion de {@link JComponent} que hace el pintado del componente
    @Override
    protected void paintComponent(Graphics g) {
        //Checkear que haya una imagen para pintar
        if ( !this.imagen.isPresent() )
            return;

        //Pintar con el tamaño completo del lienzo
        g.drawImage( this.imagen.get(), 0, 0, this.getWidth(), this.getHeight(), null );
    }
}
