package org.fpalacios.spaceinvaders;

import java.awt.EventQueue;


import org.fpalacios.spaceinvaders.interfaz.VentanaPrincipal;

import org.fpalacios.spaceinvaders.interfaz.vistas.MenuPrincipal;
import org.fpalacios.spaceinvaders.interfaz.vistas.Partida;

/**
 * Flujo de la Aplicacion con el juego
 *
 * @author Federico Palacios<fedenator7@gmail.com>
 */
public class Aplicacion {

    /*-------------------- Atributos --------------------*/
    private VentanaPrincipal ventana;

    /*-------------------- Constructores ----------------*/
    public Aplicacion() {
        EventQueue.invokeLater( () -> {
            this.ventana = new VentanaPrincipal("Space Invaders");

            // Poner vista inicial
            this.irAlMenuPrincipal();
        });
    }

    //Cambios de vistas
    public void irAlMenuPrincipal() {
        this.ventana.cambiarVistaActual( new MenuPrincipal(this) );
    }
    public void iniciarPartida() {
        this.ventana.cambiarVistaActual( new Partida(this) );
    }


    public static void main(String[] args) {
        //Simplemente inicializamos la applicacion
        new Aplicacion();
    }
}
