package org.fpalacios.spaceinvaders.motor;

import java.util.Iterator;
import java.util.ArrayList;


import org.fpalacios.spaceinvaders.motor.entidades.Entidad;

/**
 * Contenedor diseñado para guardar las entidades del juego.
 * Las operaciones de agregar/remover suceden en un buffer para no
 * modificar el contenedor mientras se esta iterando, lo causa errores.
 * Para que los cambios en el buffer se hagan efectivos, hace falta
 * llamar al metodo {@link sincronizar()} cuando no se esté iterando.
 */
public class ContenedorDeEntidades implements Iterable<Entidad> {

    /*---------------------------- Atributos ---------------------------------*/
    private ArrayList<Entidad> entidades = new ArrayList<>();

    private ArrayList<Entidad> bufferDeAgregar = new ArrayList<>();
    private ArrayList<Entidad> bufferDeRemover = new ArrayList<>();

    /*---------------------------- Funciones ---------------------------------*/
    @Override
    public Iterator<Entidad> iterator() {
        return this.entidades.iterator();
    }

    public int cantidad() {
        return this.entidades.size();
    }

    public Entidad obtener(int posicion) {
        return this.entidades.get(posicion);
    }

    public void agregar(Entidad... entidades) {
        for (Entidad entidad : entidades) {
            this.bufferDeAgregar.add(entidad);
        }
    }

    public void remover(Entidad... entidades) {
        for (Entidad entidad : entidades) {
            this.bufferDeRemover.add(entidad);
        }
    }

    public void limpiar() {
        this.remover( this.entidades.toArray(new Entidad[this.entidades.size()]) );
    }

    public void sincronizar() {
        this.entidades.addAll   (this.bufferDeAgregar);
        this.entidades.removeAll(this.bufferDeRemover);

        for (Entidad entidad : this.bufferDeRemover) {
            entidad.alRemoverse();
        }

        this.bufferDeAgregar.clear();
        this.bufferDeRemover.clear();
    }
}
