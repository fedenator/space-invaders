package org.fpalacios.spaceinvaders.motor;

import java.util.Queue;
import java.util.Optional;
import java.util.LinkedList;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;


import org.fpalacios.spaceinvaders.motor.fisicas.Vector;
import org.fpalacios.spaceinvaders.motor.fisicas.Poligono;
import org.fpalacios.spaceinvaders.motor.fisicas.Rectangulo;

import org.fpalacios.spaceinvaders.motor.entidades.Entidad;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadFisica;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadLogica;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadGrafica;

/**
 * Motor que coordina las entidades del juego
 */
public class MotorDeJuego {
    /*-------------------------- Constantes ----------------------------------*/
    // Se da informacion una vez por segundo
    private static final long NANOS_ENTRE_INFORMACIONES = 1000000000l;

    /*--------------------------- Atributos ----------------------------------*/
    private Configuracion configuracion = new Configuracion();

    // Controles
    public final Teclado teclado = new Teclado();

    private Thread        hiloDelJuego;
    private Pintor        pintor;
    private BufferedImage buffer;

    // Estructura especializada para poder guardar las entidades
    private ContenedorDeEntidades entidades = new ContenedorDeEntidades();

    // Eventos que se ejecutan antes de actualizar
    // Se sincronizan las entidades entre cada evento
    // Se usa por ejemplo para que al terminar un nivel se puedan limpiar las
    // entidades, sincronizar y despues iniciar el siguiente nivel
    public final Queue<Runnable> eventosPostSync = new LinkedList<>();

    /*--------------------- Constructores -------------*/
    public MotorDeJuego(Pintor pintor) {
        this.pintor = pintor;

        this.buffer = new BufferedImage(
            this.configuracion.anchoBuffer,
            this.configuracion.altoBuffer,
            BufferedImage.TYPE_INT_RGB
        );
    }

    /*-------------------- Funciones ------------------*/
    public void empezar() {
        this.hiloDelJuego = new Thread( () -> this.cicloDeJuego() );
        this.hiloDelJuego.start();
    }

    public void agregarEntidades(Entidad... entidades) {
        this.entidades.agregar(entidades);
    }
    public void removerEntidades(Entidad... entidades) {
        this.entidades.remover(entidades);
    }
    public void limpiarEntidades() {
        this.entidades.limpiar();
    }

    private void cicloDeJuego() {
        final long nanosEntreActualizaciones = 1000000000l / this.configuracion.apsDeseados;
        final long nanosEntreFrames          = 1000000000l / this.configuracion.fpsDeseados;

        // Nanos transcurridos hasta el inicio del ultimo ciclo que sucedio hasta ahora.
        // El valor inicial es el tiempo hasta el inicio de {@code cicloDeJuego()} para
        // que al entrar al primer ciclo el tiempo transcurrido desde el ultimo ciclo sea 0.
        long ultimoCiclo = System.nanoTime();

        long cronometroActualizacion = 0; // Nanos desde la ultima actualizacion
        long cronometroPintado       = 0; // Nanos desde el ultimo pintado
        long cronometroInformacion   = 0; // Nanos desde la ultima vez que se mostro informacion

        int contadorFps = 0;
        int contadorAps = 0;

        while(true) {
            // Nanos transcurridos hasta el inicio de este ciclo
            long cicloActual = System.nanoTime();

            // El tiempo que paso desde el ultimo ciclo hasta ahora
            long tiempoTranscurrido = cicloActual - ultimoCiclo;

            // Actualizamos los cronometros
            cronometroActualizacion += tiempoTranscurrido;
            cronometroPintado       += tiempoTranscurrido;
            cronometroInformacion   += tiempoTranscurrido;

            if (cronometroActualizacion >= nanosEntreActualizaciones)  {
                this.actualizar(cronometroActualizacion);
                contadorAps += 1;
                cronometroActualizacion = 0;
            }

            if (cronometroPintado >= nanosEntreFrames) {
                this.pintar();
                contadorFps += 1;
                cronometroPintado = 0;
            }

            if (this.configuracion.imprimirInfoGameloop) {
                if (cronometroInformacion >= NANOS_ENTRE_INFORMACIONES) {
                    System.out.println("APS: " + contadorAps);
                    System.out.println("FPS: " + contadorFps);

                    contadorAps           = 0;
                    contadorFps           = 0;
                    cronometroInformacion = 0;
                }
            }

            ultimoCiclo = cicloActual;
        }
    }

    private void detectarColisiones() {
        for (int i = 0; i < this.entidades.cantidad(); i++) {

            Entidad primeraEntidad = entidades.obtener(i);

            if ( !(primeraEntidad instanceof EntidadFisica) )
                continue;

            EntidadFisica primeraEntidadFisica = (EntidadFisica) primeraEntidad;

            Poligono   primerHitbox        = primeraEntidadFisica.obtenerCajaColisiones();
            Rectangulo primeraAproximacion = primerHitbox.obtenerAproximacion();

            for (int j = i+1; j < this.entidades.cantidad(); j++) {

                Entidad segundaEntidad = entidades.obtener(j);

                if ( !(segundaEntidad instanceof EntidadFisica) )
                    continue;

                EntidadFisica segundaEntidadFisica = (EntidadFisica) segundaEntidad;

                Poligono   segundaHitbox       = segundaEntidadFisica.obtenerCajaColisiones();
                Rectangulo segundaAproximacion = segundaHitbox.obtenerAproximacion();

                if ( !primeraAproximacion.detectarColision(segundaAproximacion) )
                    continue;

                Optional<Vector> mtvOpt = primerHitbox.detectarColision(segundaHitbox);

                if ( mtvOpt.isPresent() ) {
                    Vector mtv = mtvOpt.get();

                    primeraEntidadFisica.alColisionar(segundaEntidadFisica, mtv);
                    segundaEntidadFisica.alColisionar(primeraEntidadFisica, mtv.clonar().invertir() );
                }
            }
        }
    }

    private void sincronizarControles() {
        this.teclado.sincronizar();
    }

    private void ejecutarEventosPostSync() {
        while ( !this.eventosPostSync.isEmpty() ) {
            Runnable evento = eventosPostSync.poll();
            evento.run();
            this.entidades.sincronizar();
        }
    }

    /**
     * @param nanos: tiempo en nanosegundos que paso desde la ultima actualizacion
     */
    private void actualizar(long nanos) {
        this.sincronizarControles();
        this.entidades.sincronizar();
        this.ejecutarEventosPostSync();

        this.detectarColisiones();

        // Se calculan todos los tiempos que se pasan a los metodos de actualizacion de las entidades
        float   milis            = (float)  (nanos * .000001f);
        int     milisRedondeados = (int)     milis;
        double  segundos         = (double) (nanos * .000000001d);

        for (Entidad entidad : this.entidades) {

            // Hay que checkear que las entidades sean instances de {@link EntidadLogica}
            // ya que no todas las entidades necesariamente implementan una logica
            if (entidad instanceof EntidadLogica) {
                EntidadLogica entidadLogica = (EntidadLogica) entidad;

                entidadLogica.actualizar( nanos            );
                entidadLogica.actualizar( milis            );
                entidadLogica.actualizar( milisRedondeados );
                entidadLogica.actualizar( segundos         );
            }
        }
    }

    private void pintarFondo(Graphics2D g) {
        g.setColor( Color.WHITE );
        g.fillRect( 0, 0, this.buffer.getWidth(), this.buffer.getHeight() );
    }

    private void pintar() {
        // TODO: separar el fondo en otro lugar
        // Pintar el fondo
        Graphics2D g = (Graphics2D) this.buffer.getGraphics();
        this.pintarFondo(g);

        for (Entidad entidad : this.entidades) {

            // Hay que checkear que las entidades sean instancias de {@link EntidadGrafica}
            // ya que no todas las entidades necesariamente tienen visual
            if (entidad instanceof EntidadGrafica) {
                ( (EntidadGrafica) entidad ).pintarse(g);
            }

            // TODO: [fpalacios] Poner esto configurable
            if (this.configuracion.pintarAproximaciones) {
                if (entidad instanceof EntidadFisica) {
                    Rectangulo rectangulo = ( (EntidadFisica) entidad).obtenerCajaColisiones().obtenerAproximacion();
                    g.setColor( Color.RED );
                    g.drawRect(
                        (int) rectangulo.x,
                        (int) rectangulo.y,
                        (int) rectangulo.ancho,
                        (int) rectangulo.alto
                    );
                }
            }
        }

        this.pintor.pintar(this.buffer);
    }
}
