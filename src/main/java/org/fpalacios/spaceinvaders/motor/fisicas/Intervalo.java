package org.fpalacios.spaceinvaders.motor.fisicas;

import java.util.Optional;

/**
 * Reprecenta un intervalo unidimencional en una recta
 */
public class Intervalo {

    /*---------------------------- Atributos ------------------------------*/
    public double inicio;
    public double fin;

    /*--------------------------- Constructores ---------------------------*/
    public Intervalo(double inicio, double fin) {
        this.inicio = inicio;
        this.fin    = fin;
    }

    /*---------------------------- Funciones ------------------------------*/
    /**
     * Detecata si este intervalo esta colisonando con otro
     *
     * @return la profundidad(valor que hay que mover el otro para que ya no esten colisionando) si existe
     */
    public Optional<Double> detectarColision(Intervalo otro) {
        boolean hayColision = false;

        if (this.inicio >= otro.inicio && this.inicio <= otro.fin)
            hayColision = true;

        if (otro.inicio >= this.inicio && otro.inicio <= this.fin)
            hayColision = true;

        if (hayColision) {
            double mtvDerecha   = this.fin - otro.inicio;
            double mtvIzquierda = otro.fin - this.inicio;

            // Retorna la profunidad mas corta
            return Optional.of(
                Math.abs(mtvDerecha) < Math.abs(mtvIzquierda) ? mtvDerecha : mtvIzquierda
            );
        }

        return Optional.empty();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(fin);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(inicio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Intervalo other = (Intervalo) obj;
		if (Double.doubleToLongBits(fin) != Double.doubleToLongBits(other.fin))
			return false;
		if (Double.doubleToLongBits(inicio) != Double.doubleToLongBits(other.inicio))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Intervalo [inicio=" + inicio + ", fin=" + fin + "]";
	}
}
