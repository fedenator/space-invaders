package org.fpalacios.spaceinvaders.motor.fisicas;

/**
 * Vector 2D
 */
public class Vector {

	/*----------------------- Constantes ----------------------------*/
	public static final Vector VECTOR_NULO = new Vector(0d, 0d);

	/*------------------------ Atributos ----------------------------*/
    public double x;
    public double y;

	/*---------------------- Constructores --------------------------*/
    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

	/*------------------------ Funciones ----------------------------*/
	/**
	 * Proyeccion vectorial de este vector sobre el vector dado
	 * tal que la A proyectado sobre B = <A, B/||B||>(B/||B||)
	 * solo si B es distinto de el vector nulo
	 *
	 * @return el vector de la proyeccion
	 */
    public Vector proyectarSobre(Vector eje) throws ArithmeticException {
		if ( eje.equals(VECTOR_NULO) )
			throw new ArithmeticException("No se puede proyectar sobre el vector nulo");

		double productoEscalar = this.productoEscalar( eje.clonar().normalizar() );

    	return eje.clonar().multiplicar(productoEscalar);
    }

	/**
	 * Producto escalar(dot product) de este vector con otro vector
	 * Notacion matematica de producto escalar de A e B: <A, B>
	 * talque <A, B> = (A.x *  B.x) + (A.y + B.y)
	 */
    public double productoEscalar(Vector otro) {
    	return (this.x * otro.x) + (this.y * otro.y);
    }

	/**
	 * Multiplica este {@Vector} por un escalar
	 *
	 * @return a si mismo para poder encadenar operaciones
	 */
	public Vector multiplicar(double escalar) {
		this.x *= escalar;
		this.y *= escalar;

		return this;
	}

	/**
	 * Le suma a esta instancia otro {@link Vector}
	 *
	 * @return a si mismo para poder encadenar operaciones
	 */
	public Vector sumar(Vector otro) {
		this.x += otro.x;
		this.y += otro.y;

		return this;
	}

	/**
	 * Le resta a esta instancia otro {@link Vector}
	 *
	 * @return a si mismo para poder encadenar operaciones
	 */
	public Vector restar(Vector otro) {
		this.x -= otro.x;
		this.y -= otro.y;

		return this;
	}

	/**
	 * Divide este {@link Vector} por un divisor
	 *
	 * @return a si mismo para poder encadenar operaciones
	 */
	public Vector dividir(double divisor) {
		this.x /= divisor;
		this.y /= divisor;

		return this;
	}

	/**
	 * Normaliza este {@link Vector}, talque lo vuelve un vector con la  misma
	 * direccion y sentido pero con norma (largo) 1;
	 *
	 * @return a si mismo para poder encadenar operaciones
	 */
    public Vector normalizar() {
    	return this.dividir( this.norma() );
    }

	/**
	 * Invierte el Vector tal que sigue en la misma direccion pero en sentido opuesto
	 *
	 * @return a si mismo para poder encadenar operaciones
	 */
	public Vector invertir() {
		return this.multiplicar(-1d);
	}

    /**
     * Norma o longitud de el vector
     * Notacion matematica de norma de A: ||A||
     */
	public double norma() {
		return Math.sqrt( (this.x * this.x) + (this.y * this.y) );
    }

	/**
	 * Util porque {@link Vector} no es inmutable
	 *
	 * @return una instancia nueva con los mismos valores que este {@link Vector}
	 */
    public Vector clonar() {
    	return new Vector(this.x, this.y);
    }



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector other = (Vector) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vector["+System.identityHashCode("")+ "]"
			+ "{x: " + this.x + ", y: " + this.y + "}";
	}

}
