package org.fpalacios.spaceinvaders.motor.fisicas;

/**
 * Reprecenta un rectangulo, a diferencias de los poligonos no puede ser rotado
 * Se usa para detectar poligonos "cercanos" en el proceso de revisar colisiones
 */
public class Rectangulo {

    // TODO: [fpalacios] quitar las variables que no hagan falta e implementar
    // una forma conveniente de obtener esta info
    public final double x, y, ancho, alto, izquierda, derecha, arriba, abajo;

    public Rectangulo(double x, double y, double ancho, double alto) {
        this.x         = x;
        this.y         = y;
        this.ancho     = ancho;
        this.alto      = alto;
        this.izquierda = x;
        this.derecha   = x + ancho;
        this.arriba    = y;
        this.abajo     = y + alto;
    }

    public boolean detectarColision(Rectangulo otro) {
        boolean otroContieneMiDerecha = (this.derecha >= otro.izquierda) && (this.derecha <= otro.derecha);
        boolean yoContengoSuDerecha   = (otro.derecha >= this.izquierda) && (otro.derecha <= this.derecha);

        boolean otroContieneMiArriba = (this.arriba >= otro.arriba) && (this.arriba <= otro.abajo);
        boolean yoContengoSuArriba   = (otro.arriba >= this.arriba) && (otro.arriba <= this.abajo);

        if (otroContieneMiDerecha || yoContengoSuDerecha) {
            if (otroContieneMiArriba || yoContengoSuArriba)
                return true;
        }

        return false;
    }
}
