package org.fpalacios.spaceinvaders.motor.fisicas;

/**
 * Vertice 2D uzado para reprecentar un poligono
 * Es un NewType de un Vector
 */
public class Vertice {

	/*--------------------------- Atributos -----------------------------*/
    public Vector vector;


	/*-------------------------- Constructores --------------------------*/
    public Vertice(double x, double y) {
        this.vector = new Vector(x, y);
    }

    /*---------------------------- Funciones ----------------------------*/

    public Vector distancia(Vertice otro) {
    	return this.vector.clonar().restar(otro.vector);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vector == null) ? 0 : vector.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertice other = (Vertice) obj;
		if (vector == null) {
			if (other.vector != null)
				return false;
		} else if (!vector.equals(other.vector))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vertice [vector=" + vector + "]";
	}

    
}
