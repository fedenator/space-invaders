package org.fpalacios.spaceinvaders.motor.fisicas;

import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import java.awt.Polygon;

import org.fpalacios.spaceinvaders.utils.Tuple;

/**
 * Figura base con la que se reprecentan las hitbox de los personajes en el juego
 */
public class Poligono {

	/*---------------------------- Atributos ------------------------------*/
    protected Vertice[] vertices;

	/*--------------------------- Constructores ---------------------------*/
    public Poligono(Vertice... vertices) {
        if (vertices.length < 2)
            throw new RuntimeException("Los poligonos necesitan por lo menos 3 vertices");

        this.vertices = vertices;
    }

	/*---------------------------- Funciones ------------------------------*/

    public void transportar(Vector vector) {
        for (Vertice vertice : this.vertices) {
            vertice.vector.x += vector.x;
            vertice.vector.y += vector.y;
        }
    }

    // TODO: [fpalacios] Convertir en lazy
    public Polygon convertirEnAwtPolygon() {
        int[] cordenadasX = new int[this.vertices.length];
        int[] cordenadasY = new int[this.vertices.length];

        for (int i = 0; i < this.vertices.length; i++) {
            cordenadasX[i] = (int) this.vertices[i].vector.x;
            cordenadasY[i] = (int) this.vertices[i].vector.y;
        }

        return new Polygon(cordenadasX, cordenadasY, this.vertices.length);
    }

    // TODO: [fpalacios] Convertir en lazy
    /**
     * Obtiene un {@link Rectangulo} que rodea este poligono
     * La idea es que revisar si la aproximacion colisiona con la aproximacion
     * de el otro polinomio antes de revisar si efectivamente colisionan
     * para optimizar ya que esto se hace durante todas las actualizaciones
     */
    public Rectangulo obtenerAproximacion() {
        double mayorX = this.vertices[0].vector.x;
        double menorX = this.vertices[0].vector.x;
        double mayorY = this.vertices[0].vector.y;
        double menorY = this.vertices[0].vector.y;

        for (Vertice vertice : vertices) {
            if      (vertice.vector.x > mayorX) mayorX = vertice.vector.x;
            else if (vertice.vector.x < menorX) menorX = vertice.vector.x;

            if      (vertice.vector.y > mayorY) mayorY = vertice.vector.y;
            else if (vertice.vector.y < menorY) menorY = vertice.vector.y;
        }

        return new Rectangulo(menorX, menorY, mayorX - menorX, mayorY - menorY);
    }

    // TODO: [fpalacios] Convertir en lazy
    /**
     * Calcula todos los ejes unicos del poligono
     */
    public Set<Vector> calcularEjesUnicos() {
        // No necesariamente van a haber la misma cantidad de ejes unicos
        // que vertices, pero es seguro que no van a haber más, y es muy probable
        // que sean exactamente la misma cantidad
        Set<Vector> ejes = new HashSet<>(this.vertices.length);

        for (int i = 0; i < this.vertices.length - 1; i++) {
            Vertice actual    = vertices[i];
            Vertice siguiente = vertices[i + 1];

            ejes.add( actual.distancia(siguiente) );
        }

        int primerIndice = 0;
        int ultimoIndice = vertices.length - 1;

        ejes.add( this.vertices[ultimoIndice].distancia(this.vertices[primerIndice]) );

        return ejes;
    }

    /**
     * Proyecta este poligono sobre un {@link Vector}
     *
     * @return El intervalo que ocupa la sombra de este poligono sobre el vector
     */
    public Intervalo proyectarSobre(Vector vector) {
        double normaPrimerVector = this.vertices[0].vector.norma();

        double inicio = normaPrimerVector;
        double fin    = normaPrimerVector;

        for (int i = 1; i < this.vertices.length; i++) {
            Vertice vertice = this.vertices[i];

            Double normaProyeccion = vertice.vector.proyectarSobre(vector).norma();

            if      (normaProyeccion < inicio) inicio = normaProyeccion;
            else if (normaProyeccion > fin   ) fin    = normaProyeccion;
        }

        return new Intervalo(inicio, fin);
    }

	/**
	 * Detecta colisiones entre poligonos usando SAT(Separating axis theorem)
	 * El teorema dice que si Proyectas cada vertice de los poligonos sobre
	 * cada eje(paralelo a los lados) unico de las figuras, despues tomo los extremos
	 * de esas proyecciones, formamos una linea y algun par de lineas proyectadas sobre
	 * alguno de los ejes no se intersectan, significa que no estan colisionando.
     *
     * @return El vector minimo que hay que transportar this para que no este
     *         colisionando con el otro, en caso de que esten colisionando
	 */
    public Optional<Vector> detectarColision(Poligono otro) {
        // Tupla con el eje sobre el cual estan colisionando y la
        // profundidad o Empty en caso de que esten colisionando.
        Optional< Tuple<Vector, Double> > mtvComponentsOpt = Optional.empty();

        Set<Vector> ejesUnicos = this.calcularEjesUnicos();
        ejesUnicos.addAll( otro.calcularEjesUnicos() );

        for (Vector eje : ejesUnicos) {
            Intervalo miIntervalo   = this.proyectarSobre(eje);
            Intervalo otroIntervalo = otro.proyectarSobre(eje);

            Optional<Double> colision = miIntervalo.detectarColision(otroIntervalo);

            if ( !colision.isPresent() ) {
                return Optional.empty();
            }

            if ( mtvComponentsOpt.isPresent() ) {
                if ( colision.get() < mtvComponentsOpt.get().b ) {
                    mtvComponentsOpt = Optional.of( new Tuple<>( eje, colision.get() ) );
                }
            } else {
                mtvComponentsOpt = Optional.of( new Tuple<>( eje, colision.get() ) );
            }
        }

        Tuple<Vector, Double> mtvComponents = mtvComponentsOpt.get();
        Vector mtv = mtvComponents.a.multiplicar(mtvComponents.b);
        return Optional.of(mtv);
    }

	@Override
	public String toString() {
		return "Poligono [vertices=" + Arrays.toString(vertices) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(vertices);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Poligono other = (Poligono) obj;
		if (!Arrays.equals(vertices, other.vertices))
			return false;
		return true;
	}

}
