package org.fpalacios.spaceinvaders.motor;

/**
 * Configuracion del motor
 */
public class Configuracion {

    // Resolucion de el buffer del motor (no necesariamente es la misma que la ventana)
    public int anchoBuffer = 1366;
    public int altoBuffer  = 768;

    public int fpsDeseados = 60;
    public int apsDeseados = 60;

    public boolean pintarAproximaciones = false;
    public boolean imprimirInfoGameloop = true;

}
