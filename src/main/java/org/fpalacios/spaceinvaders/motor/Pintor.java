package org.fpalacios.spaceinvaders.motor;

import java.awt.image.BufferedImage;

/**
 * Representa la habilidad de poder pintar una imagen.
 *
 * Se utiliza para que el motor delegue el pintado de los frames a otros
 * objetos.
 */
public interface Pintor {
    public void pintar(BufferedImage imagen);
}
