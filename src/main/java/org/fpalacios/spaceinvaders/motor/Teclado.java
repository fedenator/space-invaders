package org.fpalacios.spaceinvaders.motor;

import java.util.Map;
import java.util.HashMap;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
 * Representa el estado actual de el teclado.
 *
 * Esta clase esta diseñada para que la aplicacion solo revise el estado de las
 * teclas desde el hilo de motor, para poder lograr hacer esto el motor
 * tiene que sincronizar el buffer con el estado antes de hacer update
 * sino el estado actual no se va a actualizar con los ultimos cambios
 */
public class Teclado implements KeyListener {

    /*-------------------------- Atributos -----------------------------------*/
    // El mapa que tiene la verdad de que teclas estan apretadas.
    // Para evitar conflictos solo debe accederse desde el hilo del motor
    private Map<String, Boolean> estadoActual = new HashMap<>();

    // En este buffer de espera se guardan los cambios de las teclas hasta que se llama
    // al metodo de sincronizar con las estadoActual.
    // El proposito de esto es que las teclas no cambien durante el actualizar de el motor.
    // Como la sincronizacion debe ser en el hilo del motor pero los eventos deben ser en el
    // hilo de pintado el acceso a esta buffer debe ser siempre sincronizado
    private Map<String, Boolean> bufferDeEspera = new HashMap<>();

    /*-------------------------- Funciones -----------------------------------*/
    public boolean teclaEstaApretada(String tecla) {
        return this.estadoActual.getOrDefault(tecla, false);
    }

    /**
     * Sincroniza el buffer de espera de los eventos con el estado actual y
     * vacia el buffer de espera.
     * Este metodo solo debe llamarse desde el hilo del motor
     */
    public void sincronizar() {
        synchronized (this.bufferDeEspera) {
            this.estadoActual.putAll(this.bufferDeEspera);
            this.bufferDeEspera.clear();
        }
    }

    /*---------------------------- Listeners ---------------------------------*/
    @Override
    public void keyPressed(KeyEvent evento) {

        String teclaApretada = Character.toString( evento.getKeyChar() );

        // Recordar que bufferDeEspera es accedido desde varios hilos
        synchronized (this.bufferDeEspera) {
            this.bufferDeEspera.put(teclaApretada, true);
        }

        // Consumir el evento significa que vos lo usaste,
        // y que no se debe propagar más a otros componentes
        evento.consume();
    }

    @Override
    public void keyReleased(KeyEvent evento) {

        String teclaSoltada = Character.toString( evento.getKeyChar() );

        // Recordar que bufferDeEspera es accedido desde varios hilos
        synchronized (this.bufferDeEspera){
            this.bufferDeEspera.put(teclaSoltada, false);
        }

        // Consumir el evento significa que vos lo usaste,
        // y que no se debe propagar más a otros componentes
        evento.consume();
    }

    @Override
    public void keyTyped(KeyEvent evento)
    {
    }

}
