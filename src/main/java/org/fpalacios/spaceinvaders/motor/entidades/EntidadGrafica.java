package org.fpalacios.spaceinvaders.motor.entidades;

import java.awt.Graphics2D;

/**
 * Reprecenta la caracteristica de poder mostrarse graficamente en el juego,
 * de forma que puede ser manejado por el motor grafico
 */
public interface EntidadGrafica {

    public void pintarse(Graphics2D g);
}
