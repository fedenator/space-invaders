package org.fpalacios.spaceinvaders.motor.entidades;

import org.fpalacios.spaceinvaders.motor.MotorDeJuego;

import org.fpalacios.spaceinvaders.utils.Notificador;


/**
 * Reprecenta toda entidad manejada por el motor del juego.
 * Puede ser referenciada de forma debil por otras entidades
 */
public abstract class Entidad {

    /*------------------------------ Atributos -------------------------------*/
    //Lista de observadores para ser notificados cuando te remueven
    public final Notificador<Entidad> notificadorRemover = new Notificador<>();

    //Referencia al motor
    protected MotorDeJuego motor;

    /*------------------------------ Constructores ---------------------------*/
    public Entidad(MotorDeJuego motor) {
        this.motor = motor;
    }

    /*-------------------------------- Funciones -----------------------------*/

    /**
     * Evento que invoca el motor despues de remover esta entidad del juego.
     * el encargado de invocar este evento es el {@link ContenedorDeEntidades}
     *
     * La implementacion base simplemente notifica a los observadores de que
     * fue retirada.
     */
     public void alRemoverse() {
         this.notificadorRemover.emitir(this);
    }
}
