package org.fpalacios.spaceinvaders.motor.entidades;

/**
 * Reprecenta la caracteristica de ser una entidad logica, de forma que puede
 * ser manejada por un motor de logicas
 */
public interface EntidadLogica {

    /**
     * @param nanos: Tiempo en nanosegundos que paso desde la ultima actualizacion.
     */
    default void actualizar(long nanos)
    {
    }

    /**
     * @param milis: Tiempo en milisegundos que paso desde la ultima actualizacion.
     *               Puede estar moderadamente redondeado.
     */
    default void actualizar(int  milis)
    {
    }

    /**
     * @param milis: Tiempo en milisegundos que paso desde la ultima actualizacion.
     */
    default void actualizar(float  milis)
    {
    }

    /**
     * @param segundos: Tiempo en segundos que paso desde la ultima actualizacion.
     */
    default void actualizar(double segundos)
    {
    }
}
