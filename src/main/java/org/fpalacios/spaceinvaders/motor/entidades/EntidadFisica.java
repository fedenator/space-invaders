package org.fpalacios.spaceinvaders.motor.entidades;

import org.fpalacios.spaceinvaders.motor.fisicas.Vector;
import org.fpalacios.spaceinvaders.motor.fisicas.Poligono;

/**
 * Reprecenta la caracteristica de poder colisionar con otras entdidades fisicas
 */
public interface EntidadFisica {

    public Poligono obtenerCajaColisiones();

    /**
     * Notifica de que hubo una colision con otra entidad fisica.
     *
     * @param otraEntidad con la cual colisiono
     * @param mtv (Minimum Translate Vector) vector que hay que transladar this para separarlos
     */
    default void alColisionar(EntidadFisica otraEntidad, Vector mtv) {
    }
}
