package org.fpalacios.spaceinvaders.io;

import java.io.File;
import java.io.IOException;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

/**
 * Utilidades estaticas para leer y escribir imagenes
 */
public class BufferedImageIO {

    public static void guardarImagen(BufferedImage imagen, String camino) {

        try {
            File archivo = new File(camino);

            ImageIO.write(imagen, "jpg", archivo);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
