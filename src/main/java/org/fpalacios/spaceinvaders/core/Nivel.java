package org.fpalacios.spaceinvaders.core;

import java.util.Arrays;
import java.util.ArrayList;

import org.fpalacios.spaceinvaders.motor.MotorDeJuego;
import org.fpalacios.spaceinvaders.motor.entidades.Entidad;

import org.fpalacios.spaceinvaders.core.entidades.personajes.NaveJugador;

import org.fpalacios.spaceinvaders.utils.Notificador;

/**
 * Nivel del SpaceInvaders, el nivel termina cuando se eliminan todos los enemigos
 */
public class Nivel {

    public final MotorDeJuego       motor;
    public final NaveJugador        jugador;
    public final ArrayList<Entidad> enemigos;
    public final Notificador<Void>  notificadorTerimar = new Notificador<>();

    public Nivel(MotorDeJuego motor, NaveJugador jugador, Entidad... enemigosArr) {
        this.motor    = motor;
        this.jugador  = jugador;
        this.enemigos = new ArrayList<>( Arrays.asList(enemigosArr) );

        motor.agregarEntidades(jugador);
        motor.agregarEntidades(enemigosArr);

        for (Entidad enemigo : enemigosArr) {
            enemigo.notificadorRemover.subscribir( (entidad) -> enemigoAlRemoverse(entidad) );
        }
    }

    private void enemigoAlRemoverse(Entidad entidad) {
        enemigos.remove(entidad);

        if ( enemigos.isEmpty() ) {
            terminar();
        }
    }

    public void terminar() {
        this.motor.eventosPostSync.add( () -> {
            this.motor.limpiarEntidades();
            this.motor.eventosPostSync.add( () -> this.notificadorTerimar.emitir(null) );
        });
    }
}
