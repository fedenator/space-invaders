package org.fpalacios.spaceinvaders.core; //TODO: [fpalacios] cambiar el packete por algo en español

import org.fpalacios.spaceinvaders.motor.MotorDeJuego;

import org.fpalacios.spaceinvaders.core.entidades.personajes.DummyTarget;
import org.fpalacios.spaceinvaders.core.entidades.personajes.NaveJugador;

/**
 * Reprecentacion de el estado del juego
 */
public class SpaceInvaders {

    /*------------------------------- Atributos ------------------------------*/
    public final NaveJugador  jugador;
    public final MotorDeJuego motor;

    /*----------------------------- Constructores ----------------------------*/
    public SpaceInvaders(MotorDeJuego motor) {
        this.motor = motor;

        this.jugador = new NaveJugador(motor, 500, 500);

        Nivel nivel1 = primerNivel(motor, jugador);
        nivel1.notificadorTerimar.subscribir( (evento1) -> {
            Nivel nivel2 = segundoNivel(motor, jugador);
            nivel2.notificadorTerimar.subscribir( (evento2) -> {
            	System.out.println("YEEEEE");
            });
        });

    }

    private Nivel primerNivel(MotorDeJuego motor, NaveJugador jugador) {
        DummyTarget enemigo = new DummyTarget(motor, 500, 100);

        return new Nivel(motor, jugador, enemigo);
    }

    private Nivel segundoNivel(MotorDeJuego motor, NaveJugador jugador) {
        DummyTarget enemigo1 = new DummyTarget(motor, 500 , 100);
        DummyTarget enemigo2 = new DummyTarget(motor, 1000, 100);

        return new Nivel(motor, jugador, enemigo1, enemigo2);
    }
}
