package org.fpalacios.spaceinvaders.core.habilidades;

public abstract class HabilidadActivable extends Habilidad {

    private long tiempoDeEnfriamiento;

    private long    cronometroEnfriamiento = 0;
    private boolean enEnfriamiento         = false;

    public HabilidadActivable(long tiempoDeEnfriamiento) {
        this.tiempoDeEnfriamiento = tiempoDeEnfriamiento;
    }

    public abstract void accion();

    public void activar() {
        if (!this.enEnfriamiento) {
            this.enEnfriamiento = true;
            this.accion();
        }
    }

    @Override
    public void actualizar(long nanos) {
        if ( enEnfriamiento ) {
            this.cronometroEnfriamiento += nanos;
        }

        if ( this.cronometroEnfriamiento >= tiempoDeEnfriamiento ) {
            this.cronometroEnfriamiento = 0;
            this.enEnfriamiento         = false;
        }
    }
}
