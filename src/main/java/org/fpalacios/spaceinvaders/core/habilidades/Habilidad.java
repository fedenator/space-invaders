package org.fpalacios.spaceinvaders.core.habilidades;

import org.fpalacios.spaceinvaders.motor.entidades.EntidadLogica;

public abstract class Habilidad implements EntidadLogica {

    public void alEquiparse()
    {
    }

    public void alRemoverse()
    {
    }
}
