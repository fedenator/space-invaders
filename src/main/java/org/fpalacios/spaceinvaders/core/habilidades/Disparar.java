package org.fpalacios.spaceinvaders.core.habilidades;

import org.fpalacios.spaceinvaders.motor.MotorDeJuego;

import org.fpalacios.spaceinvaders.core.entidades.personajes.NaveJugador;
import org.fpalacios.spaceinvaders.core.entidades.BalaSimple;

import org.fpalacios.spaceinvaders.motor.fisicas.Rectangulo;


public class Disparar extends HabilidadActivable {

    private static final long ENFRIAMIENTO = 300000000l;

    private NaveJugador  naveJugador;
    private MotorDeJuego motor;

    public Disparar(MotorDeJuego motor, NaveJugador naveJugador) {
        super(ENFRIAMIENTO);

        this.motor       = motor;
        this.naveJugador = naveJugador;
    }

    @Override
    public void accion() {
        Rectangulo rectangulo = this.naveJugador.obtenerCajaColisiones().obtenerAproximacion();

        double y = rectangulo.arriba - BalaSimple.ALTO - 2;
        double x = rectangulo.izquierda + (NaveJugador.ANCHO / 2d);

        this.motor.agregarEntidades( new BalaSimple(motor, x, y) );
    }
}
