package org.fpalacios.spaceinvaders.core.entidades.personajes;

import java.awt.Graphics2D;
import java.awt.Color;


import org.fpalacios.spaceinvaders.motor.entidades.EntidadFisica;

import org.fpalacios.spaceinvaders.motor.MotorDeJuego;

import org.fpalacios.spaceinvaders.motor.fisicas.Vector;
import org.fpalacios.spaceinvaders.motor.fisicas.Vertice;
import org.fpalacios.spaceinvaders.motor.fisicas.Poligono;


public class DummyTarget extends Criatura {

    /*----------------------------- Constantes -------------------------------*/
    // Dimensiones
    public static final double ANCHO = 100d;
    public static final double ALTO  = 100d;

    public static final Color IDLE        = Color.GREEN;  // Color en estado normal
    public static final Color COLSIONANDO = Color.YELLOW; // Color al estar colisionando

    public static final double VIDA_MAXIMA = 5;

    /*------------------------------ Atributos -------------------------------*/
    private Color   color         = DummyTarget.IDLE;
    private boolean coliRefresher = false; // Variable que se refreshea cuando esta colisionando


    public DummyTarget(MotorDeJuego motor, double x, double y) {
        super(motor, crearPoligonoInicial(x, y), VIDA_MAXIMA);
    }

    public static Poligono crearPoligonoInicial(double x, double y) {
        return new Poligono(
            new Vertice(x        , y       ), // Esquina arriba izquierda
            new Vertice(x + ANCHO, y       ), // Esquina arriba derecha
            new Vertice(x + ANCHO, y + ALTO), // Esquina abajo derecha
            new Vertice(x        , y + ALTO)  // Esquina abajo izquierda
        );
    }

    @Override
    public void actualizar(long nanos) {
        if (this.coliRefresher) {
            this.color         = DummyTarget.COLSIONANDO;
            this.coliRefresher = false;
        } else {
            this.color = DummyTarget.IDLE;
        }
    }

    @Override
    public void pintarModelo(Graphics2D g) {
        g.setColor   ( this.color                          );
        g.fillPolygon( this.modelo.convertirEnAwtPolygon() );
    }

    @Override
    public Poligono obtenerCajaColisiones() {
        return this.modelo;
    }

    @Override
    public void alColisionar(EntidadFisica otraEntidad, Vector mtv) {
        this.coliRefresher = true;
    }
}
