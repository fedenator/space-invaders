package org.fpalacios.spaceinvaders.core.entidades;

import java.awt.Color;
import java.awt.Graphics2D;


import org.fpalacios.spaceinvaders.motor.MotorDeJuego;

import org.fpalacios.spaceinvaders.motor.entidades.Entidad;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadLogica;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadGrafica;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadFisica;

import org.fpalacios.spaceinvaders.motor.fisicas.Poligono;
import org.fpalacios.spaceinvaders.motor.fisicas.Vertice;
import org.fpalacios.spaceinvaders.motor.fisicas.Vector;

import org.fpalacios.spaceinvaders.core.entidades.personajes.Criatura;

/**
 * Un disparo basico para hacer pruebas
 */
public class BalaSimple extends Entidad implements EntidadLogica, EntidadGrafica, EntidadFisica {

    /*----------------------------- Constantes -------------------------------*/
    private static final Vector VELOCIDAD_POR_SEGUNDO = new Vector(0d, -400d);

    // Dimensiones
    public static final double ANCHO = 20d;
    public static final double ALTO  = 20d;

    public static final double DMG = 1;

    /*------------------------------ Atributos -------------------------------*/
    public final Poligono cajaColisiones;

    /*----------------------------- Constructores ----------------------------*/
    public BalaSimple(MotorDeJuego motor, double x, double y) {
        super(motor);

        this.cajaColisiones = new Poligono(
            new Vertice(x        , y       ), // Esquina arriba izquierda
            new Vertice(x + ANCHO, y       ), // Esquina arriba derecha
            new Vertice(x + ANCHO, y + ALTO), // Esquina abajo derecha
            new Vertice(x        , y + ALTO)  // Esquina abajo izquierda
        );
    }

    /*------------------------------- Funciones ------------------------------*/
    @Override
    public void actualizar(double segundos) {
        this.cajaColisiones.transportar( VELOCIDAD_POR_SEGUNDO.clonar().multiplicar(segundos) );
    }

    @Override
    public void pintarse(Graphics2D g) {
        g.setColor   ( Color.GREEN                                 );
        g.fillPolygon( this.cajaColisiones.convertirEnAwtPolygon() );
    }

    @Override
    public Poligono obtenerCajaColisiones() {
        return this.cajaColisiones;
    }

    @Override
    public void alColisionar(EntidadFisica otraEntidad, Vector mtv) {
        if ( !(otraEntidad instanceof Criatura) )
            return;

        Criatura objetivo = (Criatura) otraEntidad;

        objetivo.recivirDmg   ( BalaSimple.DMG );
        motor.removerEntidades( this           );
    }
}
