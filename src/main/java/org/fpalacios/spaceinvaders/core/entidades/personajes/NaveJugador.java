package org.fpalacios.spaceinvaders.core.entidades.personajes;

import java.awt.Color;
import java.awt.Graphics2D;


import org.fpalacios.spaceinvaders.core.habilidades.Disparar;

import org.fpalacios.spaceinvaders.core.entidades.personajes.Criatura;

import org.fpalacios.spaceinvaders.motor.Teclado;
import org.fpalacios.spaceinvaders.motor.MotorDeJuego;

import org.fpalacios.spaceinvaders.motor.fisicas.Vector;
import org.fpalacios.spaceinvaders.motor.fisicas.Vertice;
import org.fpalacios.spaceinvaders.motor.fisicas.Poligono;


/**
 * La nave que controla el jugador
 */
public class NaveJugador extends Criatura {

    /*----------------------------- Constantes -------------------------------*/
    // La cantidad de pixeles que se puede mover por segundo
    private static final double VELOCIDAD_POR_SEGUNDO = 200d;

    // Dimensiones
    public static final double ANCHO = 100d;
    public static final double ALTO  = 100d;

    public static final double VIDA_MAXIMA = 3;

    /*----------------------------- Atributos --------------------------------*/
    private Disparar disparar;

    /*--------------------------- Constructores ------------------------------*/
    public NaveJugador(MotorDeJuego motor, double x, double y) {
        super(motor, crearPoligonoInicial(x, y), VIDA_MAXIMA );

        this.disparar = new Disparar(motor, this);

        this.agregarHabilidades(this.disparar);
    }

    /*--------------------------- Funciones ----------------------------------*/
    public static Poligono crearPoligonoInicial(double x, double y) {
        return new Poligono(
            new Vertice(x        , y       ),
            new Vertice(x + ANCHO, y       ),
            new Vertice(x + ANCHO, y + ALTO),
            new Vertice(x        , y + ALTO)
        );
    }

    @Override
    public void actualizar(double segundos) {
        super.actualizar(segundos);

        Teclado teclado = this.motor.teclado;

        if ( teclado.teclaEstaApretada("a") ) {
            this.modelo.transportar( new Vector( -(segundos * VELOCIDAD_POR_SEGUNDO), 0 ) );
        }

        if ( teclado.teclaEstaApretada("d") ) {
            this.modelo.transportar( new Vector( segundos * VELOCIDAD_POR_SEGUNDO, 0 ) );
        }

        if (teclado.teclaEstaApretada("w") ) {
            this.modelo.transportar( new Vector( 0, -(segundos * VELOCIDAD_POR_SEGUNDO) ) );
        }

        if ( teclado.teclaEstaApretada("s") ) {
            this.modelo.transportar( new Vector( 0, segundos * VELOCIDAD_POR_SEGUNDO) );
        }

        if ( teclado.teclaEstaApretada("k") ) {
            this.disparar.activar();
        }

    }

    @Override
    public void pintarModelo(Graphics2D g) {
        g.setColor   ( Color.ORANGE                        );
        g.fillPolygon( this.modelo.convertirEnAwtPolygon() );
    }

    @Override
    public Poligono obtenerCajaColisiones() {
        return this.modelo;
    }
}
