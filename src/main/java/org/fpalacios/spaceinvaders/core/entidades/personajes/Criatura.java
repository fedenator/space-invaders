package org.fpalacios.spaceinvaders.core.entidades.personajes;

import java.util.List;
import java.util.ArrayList;

import java.awt.Color;
import java.awt.Graphics2D;


import org.fpalacios.spaceinvaders.core.habilidades.Habilidad;

import org.fpalacios.spaceinvaders.motor.MotorDeJuego;

import org.fpalacios.spaceinvaders.motor.fisicas.Poligono;
import org.fpalacios.spaceinvaders.motor.fisicas.Rectangulo;

import org.fpalacios.spaceinvaders.motor.entidades.Entidad;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadLogica;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadFisica;
import org.fpalacios.spaceinvaders.motor.entidades.EntidadGrafica;

public abstract class Criatura extends Entidad implements EntidadGrafica, EntidadLogica, EntidadFisica {

    protected final List<Habilidad> habilidades = new ArrayList<>();

    // Usar el cambiarVidaActual
    private double vidaActual;
    private float  fraccionVidaActual; // Numero del 0 al 1 que guarda vidaMaxima / vidaActual

    protected double vidaMaxima;

    protected final Poligono modelo;

    public Criatura(
        MotorDeJuego motor,
        Poligono     modelo,
        double       vidaMaxima)
    {
        super(motor);

        this.vidaMaxima = vidaMaxima;
        this.cambiarVidaActual(vidaMaxima);

        this.modelo = modelo;
    }

    @Override
    public void actualizar(long nanos) {
        for (Habilidad habilidad : this.habilidades) {
            habilidad.actualizar(nanos);
        }
    }

    public void agregarHabilidades(Habilidad... habilidades) {
        for (Habilidad habilidad : habilidades) {
            this.habilidades.add(habilidad);
        }
    }

    public void recivirDmg(double dmg) {
        double vidaActualNueva = this.vidaActual - dmg;

        this.cambiarVidaActual(vidaActualNueva);

        if (vidaActualNueva <= 0) {
            this.morir();
        }
    }

    public void morir() {
        this.alMorir();
        this.motor.removerEntidades(this);
    }

    /**
     * Setter para la vida actual, cambia:
     * <ul>
     *     <li>vidaActual</li>
     *     <li>fraccionVidaActual</li>
     * </ul>
     */
    public void cambiarVidaActual(double nuevoValor) {
        this.fraccionVidaActual = (float) (nuevoValor / this.vidaMaxima);
        this.vidaActual         = nuevoValor;
    }

    @Override
    public void pintarse(Graphics2D g) {
        this.pintarModelo   (g);
        this.pintarBarraVida(g);
    }

    /**
     * Funcion que pinta la visual del personaje, otros adornos como la barra de vida
     * son pintandos automaticamente por el ciclo de pintado ver {@link pintarse(Graphics2D g) }
     */
    protected abstract void pintarModelo(Graphics2D g);

    protected void pintarBarraVida(Graphics2D g) {
        Rectangulo aproximacion = this.obtenerCajaColisiones().obtenerAproximacion();

        int anchoBarraVerde = (int) (aproximacion.ancho * this.fraccionVidaActual);
        int anchoBarraRoja  = ( (int) aproximacion.ancho ) - anchoBarraVerde;

        // Pintar el borde
        g.setColor(Color.BLACK);
        g.drawRect(
            (int) aproximacion.izquierda - 1,
            (int) aproximacion.abajo + 5,
            (int) aproximacion.ancho + 1,
            12
        );

        // Pintar la vida
        g.setColor(Color.GREEN);
        g.fillRect(
            (int) aproximacion.izquierda,
            (int) aproximacion.abajo + 6,
            anchoBarraVerde,
            10
        );

        // Pintar la vida faltante
        g.setColor(Color.RED);
        g.fillRect(
            (int) aproximacion.izquierda + anchoBarraVerde,
            (int) aproximacion.abajo + 6,
            anchoBarraRoja,
            10
        );
    }


    /*------------------- Eventos -------------------*/
    public void alMorir() {}
}
