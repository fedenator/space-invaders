package org.fpalacios.spaceinvaders.motor.fisicas;

import java.util.Optional;

import org.junit.Test;
import org.junit.Assert;

import org.fpalacios.spaceinvaders.motor.fisicas.Vector;

public class PoligonoTest {

    @Test
    public void detectarColision_iguales_true() {
        Poligono poligono1 = new Poligono(
            new Vertice(1d, 1d),
            new Vertice(2d, 2d),
            new Vertice(3d, 1d)
        );

        Poligono poligono2 = new Poligono(
            new Vertice(1d, 1d),
            new Vertice(2d, 2d),
            new Vertice(3d, 1d)
        );

        Optional<Vector> colision1 = poligono1.detectarColision(poligono2);
        Optional<Vector> colision2 = poligono1.detectarColision(poligono1);

        Assert.assertTrue( colision1.isPresent() );
        Assert.assertTrue( colision2.isPresent() );
    }

    @Test
    public void detectarColision_noColisionando_false() {
        Poligono poligono1 = new Poligono(
            new Vertice(1d, 1d),
            new Vertice(2d, 2d),
            new Vertice(3d, 1d)
        );

        Poligono poligono2 = new Poligono(
            new Vertice(4d, 1d),
            new Vertice(5d, 2d),
            new Vertice(6d, 1d)
        );

        Optional<Vector> colision1 = poligono1.detectarColision(poligono2);
        Optional<Vector> colision2 = poligono2.detectarColision(poligono1);

        Assert.assertFalse( colision1.isPresent() );
        Assert.assertFalse( colision2.isPresent() );
    }

    @Test
    public void detectarColision_colisionIntermedia_true() {
        Poligono poligono1 = new Poligono(
            new Vertice(1d, 1d),
            new Vertice(2d, 2d),
            new Vertice(3d, 1d)
        );

        Poligono poligono2 = new Poligono(
            new Vertice(2d, 1d),
            new Vertice(3d, 2d),
            new Vertice(4d, 1d)
        );

        Optional<Vector> colision1 = poligono1.detectarColision(poligono2);
        Optional<Vector> colision2 = poligono2.detectarColision(poligono1);

        Assert.assertTrue( colision1.isPresent() );
        Assert.assertTrue( colision2.isPresent() );
    }
}
