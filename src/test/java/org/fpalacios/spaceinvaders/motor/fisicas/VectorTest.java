package org.fpalacios.spaceinvaders.motor.fisicas;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Pruebas basicas de la matematica de vectores
 */
public class VectorTest {

	@Test
	public void norma_X3Y4_5() {
		Vector vector = new Vector(3d, 4d);

		assertEquals( 5d, vector.norma(), 0d );
	}
	
	@Test
	public void normalizar_X4Y3_X08Y06() {
		Vector vector = new Vector(4d, 3d);

		assertEquals( new Vector(.8d, .6d), vector.normalizar() );
	}

	@Test
	public void dividir_X4Y2dividido2_X2Y1() {
		Vector vector = new Vector(4d, 2d);

		assertEquals( new Vector(2d, 1d), vector.dividir(2d) );
	}

	@Test
	public void multiplicar_X4Y2multiplicado2_X8Y4() {
		Vector vector = new Vector(4d, 2d);

		assertEquals( new Vector(8d, 4d), vector.multiplicar(2d) );
	}

	@Test
	public void productoEscalar_X4Y3productoEscalarX5Y2_26() {
		Vector vector = new Vector(4d, 3d);

		assertEquals( 26d, vector.productoEscalar( new Vector(5d, 2d) ), 0d );
	}
	
}