package org.fpalacios.spaceinvaders.motor.fisicas;

import org.junit.Test;
import org.junit.Assert;

public class VerticeTest {

    @Test
    public void distancia_X1Y1conX1Y2_X0Yn1() {
        Vertice vertice1 = new Vertice(1d, 1d);
        Vertice vertice2 = new Vertice(1d, 2d);

        Vector distancia = vertice1.distancia(vertice2);

        Vector distanciaEsperada = new Vector(0d, -1d);

        Assert.assertEquals(distanciaEsperada, distancia);
    }

    @Test
    public void distancia_X1Y2conX1Y1_X0Y1() {
        Vertice vertice1 = new Vertice(1d, 2d);
        Vertice vertice2 = new Vertice(1d, 1d);

        Vector distancia = vertice1.distancia(vertice2);

        Vector distanciaEsperada = new Vector(0d, 1d);

        Assert.assertEquals(distanciaEsperada, distancia);
    }
}
