package org.fpalacios.spaceinvaders.motor.fisicas;

import java.util.Optional;

import org.junit.Test;
import org.junit.Assert;

public class IntervaloTest {

    @Test
    public void detectarColision_separadosLejos_false() {
        Intervalo intervalo1 = new Intervalo(1.4142135623730951d, 3.9999999999999996d);
        Intervalo intervalo2 = new Intervalo(4.123105625617661d , 6.999999999999999d);

        Optional<Double> colision1 = intervalo1.detectarColision(intervalo2);
        Optional<Double> colision2 = intervalo2.detectarColision(intervalo1);

        Assert.assertFalse( colision1.isPresent() );
        Assert.assertFalse( colision2.isPresent() );
    }

    @Test
    public void detectarColision_colisionSimple_true() {
        Intervalo intervalo1 = new Intervalo(0d, 2d);
        Intervalo intervalo2 = new Intervalo(1d, 3d);

        Optional<Double> colision1 = intervalo1.detectarColision(intervalo2);
        Optional<Double> colision2 = intervalo2.detectarColision(intervalo1);

        Assert.assertTrue( colision1.isPresent() );
        Assert.assertTrue( colision2.isPresent() );
    }

    @Test
    public void detectarColision_colisionEnUnBorde_true() {
        Intervalo intervalo1 = new Intervalo(0d, 1d);
        Intervalo intervalo2 = new Intervalo(1d, 3d);

        Optional<Double> colision1 = intervalo1.detectarColision(intervalo2);
        Optional<Double> colision2 = intervalo2.detectarColision(intervalo1);

        Assert.assertTrue( colision1.isPresent() );
        Assert.assertTrue( colision2.isPresent() );
    }

    @Test
    public void detectarColision_colisionInterna_true() {
        Intervalo intervalo1 = new Intervalo(0d, 3d);
        Intervalo intervalo2 = new Intervalo(1d, 2d);

        Optional<Double> colision1 = intervalo1.detectarColision(intervalo2);
        Optional<Double> colision2 = intervalo2.detectarColision(intervalo1);

        Assert.assertTrue( colision1.isPresent() );
        Assert.assertTrue( colision2.isPresent() );
    }
}
